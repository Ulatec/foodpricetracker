package FoodPriceTracker.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Store {
    @javax.persistence.Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long Id;
    @OneToMany(fetch = FetchType.EAGER)
    private List<DailyData> dailyData = new ArrayList<>();
    private int zip;
    private String name;

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Store(){

    }
    public Store(int zip, String name) {
        this.zip = zip;
        this.name = name;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public List<DailyData> getDailyData() {
        return dailyData;
    }

    public void setDailyData(List<DailyData> dailyData) {
        this.dailyData = dailyData;
    }
}
