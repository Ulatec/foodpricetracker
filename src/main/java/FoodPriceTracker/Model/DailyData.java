package FoodPriceTracker.Model;

import javax.persistence.*;
import java.util.*;

@Entity
public class DailyData {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @OneToMany(fetch = FetchType.EAGER)
    private Set<DataPoint> datapoints = new HashSet<>();
    private Date date;
    @ManyToOne
    private Store store;


    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public DailyData(){

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<DataPoint> getDatapoints() {
        return datapoints;
    }

    public void setDatapoints(Set<DataPoint> datapoints) {
        this.datapoints = datapoints;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DailyData dailyData = (DailyData) o;
        return Objects.equals(id, dailyData.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
