package FoodPriceTracker.Model;

import javax.persistence.*;
import java.util.*;

@Entity
public class Item {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private String NameMatch;
    private String url;
    public Item(){

    }
    public Item(String name, String nameMatch, String url){
        this.name = name;
        this.NameMatch = nameMatch;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNameMatch() {
        return NameMatch;
    }

    public void setNameMatch(String nameMatch) {
        NameMatch = nameMatch;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", NameMatch='" + NameMatch + '\'' +
                '}';
    }
}
