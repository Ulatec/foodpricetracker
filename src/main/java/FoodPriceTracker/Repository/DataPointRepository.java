package FoodPriceTracker.Repository;

import FoodPriceTracker.Model.DailyData;
import FoodPriceTracker.Model.DataPoint;
import FoodPriceTracker.Model.Item;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface DataPointRepository extends PagingAndSortingRepository<DataPoint, Long> {
    List<DataPoint> findAllByItem(Item item);
}