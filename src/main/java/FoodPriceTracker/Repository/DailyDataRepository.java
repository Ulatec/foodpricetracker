package FoodPriceTracker.Repository;

import FoodPriceTracker.Model.DailyData;
import FoodPriceTracker.Model.DataPoint;
import FoodPriceTracker.Model.Item;
import FoodPriceTracker.Model.Store;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Date;
import java.util.List;

@RepositoryRestResource
public interface DailyDataRepository extends PagingAndSortingRepository<DailyData, Long> {
    List<DailyData> findAllByStore(Store store);
    List<DailyData> findAllByStoreAndDate(Store store, Date date);
}