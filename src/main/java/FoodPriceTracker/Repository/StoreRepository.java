package FoodPriceTracker.Repository;

import FoodPriceTracker.Model.Item;
import FoodPriceTracker.Model.Store;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface StoreRepository extends PagingAndSortingRepository<Store, Long> {
    List<Store> findByName(String name);
}