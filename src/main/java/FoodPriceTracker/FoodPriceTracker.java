package FoodPriceTracker;

import FoodPriceTracker.Model.DailyData;
import FoodPriceTracker.Model.DataPoint;
import FoodPriceTracker.Model.Item;
import FoodPriceTracker.Model.Store;
import FoodPriceTracker.Repository.DailyDataRepository;
import FoodPriceTracker.Repository.DataPointRepository;
import FoodPriceTracker.Repository.ItemRepository;
import FoodPriceTracker.Repository.StoreRepository;
import org.apache.commons.collections.ArrayStack;
/*{*/
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static java.lang.Thread.sleep;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

@Component
public class FoodPriceTracker implements ApplicationRunner {
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private DataPointRepository dataPointRepository;
    @Autowired
    private DailyDataRepository dailyDataRepository;
    @Autowired
    private StoreRepository storeRepository;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<Item> startingItems = new ArrayList<>();
        List<Store> startingStores = new ArrayList<>();
        Item item1 = new Item("Apple", "Gala Apples, each", "https://grocery.walmart.com/ip/Gala-Apples-each/44390953");
        Item item2 = new Item("Bread", "Great Value White Bread, 20 oz", "https://grocery.walmart.com/ip/Great-Value-White-Bread-20-oz/10315752");
        Item item3 = new Item("Milk", "Great Value 2% Reduced-Fat Milk, 1 Gallon, 128 Fl. Oz.", "https://grocery.walmart.com/ip/Great-Value-2-Reduced-Fat-Milk-1-Gallon-128-Fl-Oz/10450115");
        Item item4 = new Item("HotDog", "Bar S Franks Made with Chicken & Pork, 12 Oz.", "https://grocery.walmart.com/ip/Bar-S-Franks-Made-with-Chicken-Pork-12-Oz/10290749");
        Item item5 = new Item("Spaghetti", "Great Value Spaghetti Pasta, 16 oz", "https://grocery.walmart.com/ip/Great-Value-Spaghetti-Pasta-16-oz/10534115");
        Item item6 = new Item("Eggs","Great Value Large White Eggs, 18 count, 36 oz", "https://grocery.walmart.com/ip/Great-Value-Large-White-Eggs-18-count-36-oz/172844767");
        Item item7 = new Item("Canned Peas", "Del Monte Sweet Peas, 15 Oz","https://grocery.walmart.com/ip/Del-Monte-Sweet-Peas-15-Oz/10318790");
        Item item8 = new Item("Strawberries", "Fresh Strawberries, 1 lb", "https://grocery.walmart.com/ip/Fresh-Strawberries-1-lb/44391605");
        Item item9 = new Item("Frozen Pizza", "DIGIORNO Original Rising Crust Pepperoni Frozen Pizza 27.5 oz. Box", "https://grocery.walmart.com/ip/DIGIORNO-Original-Rising-Crust-Pepperoni-Frozen-Pizza-27-5-oz-Box/25826933");
        startingItems.add(item1);
        startingItems.add(item2);
        startingItems.add(item3);
        startingItems.add(item4);
        startingItems.add(item5);
        startingItems.add(item6);
        startingItems.add(item7);
        startingItems.add(item8);
        startingItems.add(item9);
        Store store1 = new Store(92395, "Victorville,CA");
        Store store2 = new Store(75038, "Irving, TX");
        Store store3 = new Store(48642, "Midland, MI");
        Store store4 = new Store(34470, "Ocala, FL");
        Store store5 = new Store(98053, "Bellevue, WA");
        Store store6 = new Store(83701, "Boise, ID");
        Store store7 = new Store(43035, "Columbus, OH");
        startingStores.add(store1);
        startingStores.add(store2);
        startingStores.add(store3);
        startingStores.add(store4);
        startingStores.add(store5);
        startingStores.add(store6);
        startingStores.add(store7);
        //List<Store> allStores = (List<Store>) storeRepository.findAll();
        for(Store store : startingStores){
            int results = storeRepository.findByName(store.getName()).size();
            if(results == 0){
                storeRepository.save(store);
            }
        }

        for(Item item : startingItems){
            List<Item> results = itemRepository.findAllByName(item.getName());
            if(results.size() == 0){
                itemRepository.save(item);
            }else{
                Item tempItem = results.get(0);
                tempItem.setUrl(item.getUrl());
                itemRepository.save(tempItem);
            }
        }

        List<Item> allItems = (List<Item>) itemRepository.findAll();
        List<Store> allStores = (List<Store>) storeRepository.findAll();
//        String[] zipCodes = {"92395",  "75038" , "48642"};
//        String[] items = {"Apple", "Milk", "Hotdog", "Shampoo", "Bread"};
//        String[] listedItemNames = {"Gala Apples, each"};

        System.setProperty("webdriver.chrome.driver", "C:/Users/Mark/Desktop/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "C:/Users/Mark/Desktop/geckodriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("C:\\Program Files (x86)\\Google\\Chrome Beta\\Application\\chrome.exe");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        FirefoxProfile firefoxProfile = new FirefoxProfile();

//        ChromeDriver chromedriver = new ChromeDriver(chromeOptions);
firefoxOptions.setProfile(firefoxProfile);
        FirefoxDriver chromedriver = new FirefoxDriver(firefoxOptions);
        WebDriverWait wait = new WebDriverWait(chromedriver, 15);
        //Set store
//        chromedriver.get("https://grocery.walmart.com/");
//        sleep(3000);
        Instant now = Instant.now();
        LocalDate localDate = now.atZone(ZoneId.systemDefault()).toLocalDate();
        for (Store store : allStores) {
            try {
                sleep(1800);
                chromedriver.get("https://grocery.walmart.com/");
                sleep(1800);
                if(chromedriver.findElementsByClassName("OnboardingModal__closeBtn___2xhJM").size()>0){
                    WebElement close = wait.until(presenceOfElementLocated(By.className("OnboardingModal__closeBtn___2xhJM")));
                    sleep(1800);
                    close.click();
                }
                sleep(3700);
                List<WebElement> WebElements =  chromedriver.findElementsByClassName("FulfillmentBanner__btn___3av1F");
                if(WebElements.size()>0){
                    WebElement element = WebElements.get(0);
                    sleep(2900);
                    element.click();
                }


                sleep(2800);
                WebElement zip_input = chromedriver.findElementByClassName("LocationFlyout__searchInput___WW5vC");
                sleep(2000);
                zip_input.clear();
                zip_input.sendKeys(String.valueOf(store.getZip()));
                sleep(1000);
                zip_input.submit();
                sleep(2800);
                WebElement storeSelection = chromedriver.findElementsByClassName("List__tile___108pZ").get(0);
                sleep(1000);
                storeSelection.click();
                sleep(1000);
                WebElement submit = chromedriver.findElementsByClassName("button-block").get(1);
                sleep(1000);
                submit.click();
                sleep(1000);
                submit = chromedriver.findElements(By.cssSelector("button[data-automation-id='confirmFulfillmentBtn']")).get(0);
                sleep(1500);
                submit.click();
                sleep(2500);
                List<WebElement> close = chromedriver.findElementsByCssSelector("svg[data-automation-id='onboardingModalCloseBtn']");
                if(close.size()>0) {
                    sleep(1500);
                    close.get(0).click();
                }
                sleep(1000);
                DailyData dailyData = new DailyData();
                dailyData.setDate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
                dailyData.setStore(store);
                Set<DataPoint> dataPoints = new HashSet<>();
                for(Item item : allItems){
                    while(true) {
                        try {
                            chromedriver.get(item.getUrl());
                            sleep(5000);
                            if(chromedriver.findElementsByCssSelector("span[data-automation-id='salePrice']").size()>0) {
                                WebElement price = chromedriver.findElementByCssSelector("span[data-automation-id='salePrice']");
                                System.out.println(price.getText());
                                if (price.getText() != null) {
                                    DataPoint dataPoint = new DataPoint();
                                    dataPoint.setItem(item);
                                    dataPoint.setPrice(Double.parseDouble(price.getText().replace("$", "")));
                                    dataPoints.add(dataPoint);
                                    dataPointRepository.save(dataPoint);
                                }
                            }
                            break;
                        }catch(Exception e){
                            System.out.println("refreshing.");
                        }
                    }
                    sleep(32000);
//                    List<WebElement> products = chromedriver.findElementByClassName("ProductsPage__productListGridColumns___1csCu").findElements(By.tagName("div"));
//                    boolean itemFound = false;
//                    for(WebElement element1 : products){
//                        if(itemFound){
//                            break;
//                        }
//                        List<WebElement> spans = element1.findElements(By.tagName("span"));
//                        for(WebElement span : spans){
//                            System.out.println(span.getText());
//                            if(span.getText().equals(item.getNameMatch())){
//                                Double priceD = null;
//                                DataPoint dataPoint = new DataPoint();
//                                if(element1.findElements(By.className("productTile__outOfStock___2Dlcs")).size()==0){
//                                    WebElement price = element1.findElement(By.cssSelector("span[data-automation-id='salePrice']"));
//                                    priceD = Double.parseDouble(price.getText().replace("$",""));
//                                    dataPoint.setPrice(priceD);
//                                }else{
//                                    System.out.println(item.getName() + " was out of stock.");
//                                }
//                                //System.out.println("WE FOUND IT");
//                                dataPoint.setItem(item);
//
//
//
//                                dataPoints.add(dataPoint);
//                                dataPointRepository.save(dataPoint);
//                                System.out.println("Price was " + priceD);
//                                itemFound = true;
//                                break;
//                            }
//                        }
//
//                    }
                }
                List<DailyData> dailyDataList = dailyDataRepository.findAllByStoreAndDate(store, Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
                if(dailyDataList.size()>0){
                    DailyData pulledData = dailyDataList.get(0);
                    Set<DataPoint> dataPoints1 = pulledData.getDatapoints();
                    for(DataPoint dataPoint: dataPoints1){
                        System.out.println(dataPoint.getItem());
                    }
                }
                List<DailyData> dailyDataList1 = store.getDailyData();
                dailyData.setDatapoints(dataPoints);
                dailyDataList1.add(dailyData);
                store.setDailyData(dailyDataList1);
                dailyDataRepository.save(dailyData);
                storeRepository.save(store);
                sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        chromedriver.quit();
        allStores = (List<Store>) storeRepository.findAll();
        for(Store store : allStores){
            for(DailyData dailyData : store.getDailyData()){
                for(DataPoint dataPoint : dailyData.getDatapoints()){
                    System.out.println(store.getName()+":"+dailyData.getDate()+":"+dataPoint.getItem().getName()+"="+dataPoint.getPrice());
                }
            }
        }
    }

}
